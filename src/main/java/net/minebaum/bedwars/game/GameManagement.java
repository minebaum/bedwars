package net.minebaum.bedwars.game;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.data.LobbyData;
import net.minebaum.bedwars.states.EndingState;
import net.minebaum.bedwars.states.IngameState;
import net.minebaum.bedwars.states.LobbyState;
import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.game.Game;
import net.minebaum.playerapi.game.ISetinventory;
import net.minebaum.playerapi.game.states.GameState;
import net.minebaum.playerapi.game.team.Team;
import net.minebaum.playerapi.mysql.MySQLConnector;

import java.util.ArrayList;

public class GameManagement {

    private static Game game;
    private static MySQLConnector connector;
    private static ArrayList<Team> teams;
    private static GameState[] states;
    public static final int LOBBY_STATE = 0,
                            INGAME_STATE = 1,
                            ENDING_STATE = 2;
    public static final int LOBBY_INV = 0,
                            INGAME_INV = 1;
    private static ISetinventory[] invSetter;

    public static Game getGame() {
        return game;
    }

    public static void setup(){
        //connector = new MySQLConnector(); TODO
        teams = new ArrayList<>();
        states = new GameState[3];
        states[LOBBY_STATE] = new LobbyState();
        states[INGAME_STATE] = new IngameState();
        states[ENDING_STATE] = new EndingState();
        invSetter = new ISetinventory[3];
        game = new Game("BedWars", connector, 2, 2, teams, BedWars.getManager().getPlugin(), new ISetinventory() {
            @Override
            public void set(MBPlayer mbPlayer) {
                ///////////// SPECTATOR INVENTORY ///////////////////
                // TODO
            }
        }).setupGameStates(states, new LobbyState());
    }

    public static void setupInventorySet(){
        invSetter[LOBBY_INV] = new ISetinventory() {
            @Override
            public void set(MBPlayer mbPlayer) {
                mbPlayer.getSpigotPlayer().getInventory().setItem(0, LobbyData.TEAM_SELECT);
                mbPlayer.getSpigotPlayer().getInventory().setItem(1, LobbyData.VOTING_ITEM);
                mbPlayer.getSpigotPlayer().getInventory().setItem(8, LobbyData.LEAE_ITEM);
            }
        };
    }
}
