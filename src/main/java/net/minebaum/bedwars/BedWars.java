package net.minebaum.bedwars;

import lombok.Getter;
import net.minebaum.bedwars.bukkit.commands.StartCommand;
import net.minebaum.bedwars.bukkit.listeners.InventoryListener;
import net.minebaum.bedwars.bukkit.listeners.JoinListener;
import net.minebaum.bedwars.map.MapHandler;
import net.minebaum.playerapi.developement.ServerManager;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class BedWars extends JavaPlugin {

    public static final String PREFIX = "§8[§cBedWars§8] §r";

    @Getter
    private static ServerManager manager;
    @Getter
    private static BedWars instance;

    private MapHandler mapHandler;

    @Override
    public void onLoad() {
        manager = new ServerManager(this, "BEDWARS");
    }

    @Override
    public void onEnable() {
        register();
    }

    private void register() {

        this.mapHandler = new MapHandler();

        // LISTENER
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new JoinListener(), this);
        pm.registerEvents(new InventoryListener(), this);

        // COMMANDS
        getCommand("start").setExecutor(new StartCommand());

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
