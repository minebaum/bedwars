package net.minebaum.bedwars.counters;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.game.GameManagement;
import net.minebaum.bedwars.map.Map;
import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.developement.Data;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

public class LobbyCountdown extends Countdown{

    private static final int IDLE_TIME = 1, COUNTDOWN_TIME = 20;

    private int idleID;
    private boolean isIdling;
    private int seconds;
    private boolean isRunning;

    public int getSeconds() {
        return seconds;
    }

    public static int getCountdownTime() {
        return COUNTDOWN_TIME;
    }

    public int getIdleID() {
        return idleID;
    }

    public static int getIdleTime() {
        return IDLE_TIME;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public void setIdling(boolean idling) {
        isIdling = idling;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public LobbyCountdown(){
        seconds = COUNTDOWN_TIME;
    }

    @Override
    public void start() {
        isRunning = true;
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(BedWars.getManager().getPlugin(), new Runnable() {
            @Override
            public void run() {
                switch (seconds){
                    case 20: case 10: case 5: case 3: case 2:
                        for(MBPlayer p : GameManagement.getGame().getPlayers()){
                            p.sendActionbar("§8« §7Das Spiel startet in §c" + seconds + " §8»");
                            p.getSpigotPlayer().setLevel(seconds);
                        }
                        if(seconds == 10) {
                            Map map = BedWars.getInstance().getMapHandler().getVoting().getWinnerMap();
                            BedWars.getInstance().getMapHandler().setFinalMap(map);
                            GameManagement.getGame().getPlayers().forEach(player -> {
                                player.getSpigotPlayer().playSound(player.getSpigotPlayer().getLocation(), Sound.ANVIL_USE, 2, 1);
                                player.sendMessage("§7-------------§8| §eMap Voting §8|§7-------------");
                                player.sendMessage(" ");
                                player.sendMessage("§7Map: " + map.getDisplayName());
                                player.sendMessage("§7Erbauer: " + map.getBuilderName());
                                player.sendMessage(" ");
                                player.sendMessage("§7-------------§8| §eMap Voting §8|§7-------------");
                            });
                        }
                        break;
                    case 1:
                        Bukkit.broadcastMessage(Data.PREFIX + "§7Das Spiel startet gleich....");
                        break;

                    case 0:
                        Bukkit.broadcastMessage(Data.PREFIX + "§7Das Spiel startet jetzt!");
                        break;

                    default:
                        break;
                }
                seconds--;
            }
        }, 0, 20);
    }

    @Override
    public void stop() {
        if(isRunning){
            Bukkit.getScheduler().cancelTask(taskID);
            isRunning = false;
            seconds = COUNTDOWN_TIME;
        }
    }

    public void startIdle(){
        isIdling = true;
        idleID = Bukkit.getScheduler().scheduleSyncRepeatingTask(BedWars.getManager().getPlugin(), new Runnable() {
            @Override
            public void run() {
                for(MBPlayer player : GameManagement.getGame().getPlayers()){
                    player.sendActionbar("§8« §cEs fehlen noch §7" + (GameManagement.getGame().getMinPlayers() - GameManagement.getGame().getPlayers().size()) + " §cSpieler §8»");
                }
            }
        }, 0, 20*IDLE_TIME);
    }

    public void stopIdle(){
        if(isIdling){
            Bukkit.getScheduler().cancelTask(idleID);
            isIdling = false;
        }
    }
}
