package net.minebaum.bedwars.counters;

public abstract class Countdown {

    protected int taskID;

    public abstract void start();
    public abstract void stop();

}
