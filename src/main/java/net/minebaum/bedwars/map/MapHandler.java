package net.minebaum.bedwars.map;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.game.GameManagement;
import net.minebaum.bedwars.map.voting.MapVoting;
import net.minebaum.bedwars.map.voting.MapVotingGui;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.ArrayList;

@Getter
public class MapHandler {

    private ArrayList<Map> maps;
    private static final File MAP_DIR = new File("//plugins//BedWars//Maps");
    private MapVoting voting;
    @Getter
    @Setter
    private Map finalMap;

    public MapHandler(){
        this.maps = new ArrayList<>();
        if(!MAP_DIR.exists()) MAP_DIR.mkdir();
        this.loadMaps();
        this.voting = new MapVoting(maps);
    }

    @SneakyThrows
    public void saveMap(Map map){
        File file = new File(MAP_DIR, "MAP-" + map.getName());
        if(!file.exists()) file.mkdir();
        FileWriter fw = new FileWriter(file);
        fw.write(map.toString());
        fw.flush();
        fw.close();
    }

    @SneakyThrows
    private void loadMaps(){
        for (int i = 0; i < MAP_DIR.listFiles().length; i++) {
            String json = new String(Files.readAllBytes(MAP_DIR.toPath()));
            Map map = Map.fromString(json);
            if(map.getMapTeams().size() == GameManagement.getGame().getTeams().size()) {
                this.maps.add(map);
            }
        }
        System.out.println("Loaded " + this.maps.size() + " Maps");
    }

    public void addMap(Map map) {
        this.maps.add(map);
    }

    public Map getMapByName(String name) {
        return this.maps.stream().filter(map -> map.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }
}
