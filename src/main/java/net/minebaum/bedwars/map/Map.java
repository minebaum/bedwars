package net.minebaum.bedwars.map;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.map.locations.MapLocation;
import net.minebaum.bedwars.spawners.SpawnerType;
import org.apache.commons.compress.utils.Lists;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Getter
public class Map {

    private final String name;
    private final String displayName;
    private final String builderName;
    private final ItemStack iconStack;
    private List<MapTeam> mapTeams = Lists.newArrayList();
    @Setter
    private MapLocation spectatorLocation;
    private HashMap<SpawnerType, List<MapLocation>> spawnerLocations;

    public Map(String name, String displayName, String builderName, ItemStack iconStack) {
        this.iconStack = iconStack;
        this.name = name;
        this.displayName = displayName;
        this.builderName = builderName;
        this.spawnerLocations = Maps.newHashMap();
    }

    public void addSpawnerLocation(SpawnerType type, MapLocation location) {
        List<MapLocation> locations = this.spawnerLocations.containsKey(type) ? this.spawnerLocations.get(type) : Arrays.asList(location);
        this.spawnerLocations.put(type, locations);
    }

    public void addTeam(MapTeam team) {
        this.mapTeams.add(team);
    }

    public MapTeam getMapTeamByName(String name) {
        return this.mapTeams.stream().filter(team -> team.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    public void finish() {
        BedWars.getInstance().getMapHandler().saveMap(this);
    }

    public boolean canFinish() {
        if(mapTeams.size() < 2 || spectatorLocation == null || spawnerLocations.isEmpty()) return false;
        for (MapTeam mapTeam : this.mapTeams) {
            if(!mapTeam.ready()) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

    public static Map fromString(String from){
        return new Gson().fromJson(from, Map.class);
    }

    public void loadWorld() {
        String worldName = this.spectatorLocation.getWorldName();
        Bukkit.createWorld(new WorldCreator(worldName));
    }
}
