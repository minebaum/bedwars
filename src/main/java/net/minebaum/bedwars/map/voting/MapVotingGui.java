package net.minebaum.bedwars.map.voting;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.map.Map;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class MapVotingGui {

    public static final String GUI_NAME = "§cMap Voting";

    private Inventory inventory;
    private MapVoting voting;

    public MapVotingGui(MapVoting voting) {
        this.voting = voting;
        this.initInventory();
    }

    private void initInventory() {
        this.inventory = Bukkit.createInventory(null, this.getSlots(), GUI_NAME);
        this.voting.getMAPS().forEach(map -> {
            ItemStack iconStack = map.getIconStack();
            ItemMeta meta = iconStack.getItemMeta();
            meta.setDisplayName("§e" + map.getDisplayName());
            meta.setLore(Arrays.asList("§7Votes: §e" + this.voting.getVotes(map)));
            iconStack.setItemMeta(meta);
            this.inventory.addItem(iconStack);
        });
    }

    private int getSlots() {
        int rows = (int) Math.ceil(Double.valueOf(this.voting.getMAPS().size()) / 9D);
        return rows * 9;
    }

    public void open(Player player) {
        player.openInventory(this.inventory);
    }

    public void updateLore(Map map) {
        ItemStack stack = null;
        for (ItemStack contentStack : this.inventory.getContents()) {
            if(contentStack != null && contentStack.getType() != Material.AIR) {
                String displayName = ChatColor.stripColor(contentStack.getItemMeta().getDisplayName());
                if(displayName.equalsIgnoreCase(map.getDisplayName())) {
                    stack = contentStack;
                    break;
                }
            }
        }
        if(stack != null) {
            ItemStack iconStack = map.getIconStack();
            ItemMeta meta = iconStack.getItemMeta();
            meta.setLore(Arrays.asList("§7Votes: §e" + this.voting.getVotes(map)));
            iconStack.setItemMeta(meta);
        }
    }
}
