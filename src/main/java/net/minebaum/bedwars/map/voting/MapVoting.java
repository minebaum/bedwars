package net.minebaum.bedwars.map.voting;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import net.minebaum.bedwars.map.Map;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class MapVoting {

    @Getter
    private final List<Map> MAPS;
    private final HashMap<Map, Integer> VOTE_MAPS = Maps.newHashMap();
    private final HashMap<Player, Map> PLAYER_VOTES = Maps.newHashMap();
    @Getter
    private final MapVotingGui gui;
    @Setter
    @Getter
    private boolean running;
    @Setter
    private Map forceMap;

    public MapVoting(List<Map> maps) {
        maps.forEach(map -> VOTE_MAPS.put(map, 0));
        this.MAPS = maps;
        this.running = true;
        this.gui = new MapVotingGui(this);
    }

    public void addVote(Player player, Map map) {
        VOTE_MAPS.put(map, getVotes(map) + 1);
        PLAYER_VOTES.put(player, map);
        this.gui.updateLore(map);
    }

    public void removeVote(Player player) {
        Map map = PLAYER_VOTES.get(player);
        VOTE_MAPS.put(map, getVotes(map) - 1);
        this.gui.updateLore(map);
    }

    public boolean hasVoted(Player player) {
        return PLAYER_VOTES.containsKey(player);
    }

    public int getVotes(Map map) {
        return this.VOTE_MAPS.get(map);
    }

    public Map getMapOfPlayer(Player player) {
        return this.PLAYER_VOTES.get(player);
    }

    public Map getWinnerMap() {
        if(forceMap != null) return forceMap;
        AtomicReference<Map> winnerMap = new AtomicReference<>(VOTE_MAPS.keySet().stream().findAny().get());
        VOTE_MAPS.forEach((map, votes) -> {
            if(this.getVotes(map) > this.getVotes(winnerMap.get())) {
                winnerMap.set(map);
            }
        });
        return winnerMap.get();
    }

}
