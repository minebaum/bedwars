package net.minebaum.bedwars.map;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

@Getter
@Setter
public class MapTeam {

    private final String name;
    private Location bedLocation1, bedLocation2;
    private Location spawnLocation;

    public MapTeam(String name) {
        this.name = name;
    }

    public boolean ready() {
        return spawnLocation != null && bedLocation1 != null && bedLocation2 != null;
    }
}
