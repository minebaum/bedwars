package net.minebaum.bedwars.shop;

import net.minebaum.bedwars.shop.item.IShopItem;
import org.bukkit.inventory.ItemStack;
import java.util.List;

public interface IShopMenu {

    List<IShopItem> initShopItems();
    ItemStack getIcon();
    String getDisplayName();

}
