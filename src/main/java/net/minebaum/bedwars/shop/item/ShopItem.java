package net.minebaum.bedwars.shop.item;

import lombok.Getter;
import net.minebaum.bedwars.spawners.SpawnerType;
import net.minebaum.playerapi.MBPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ShopItem implements IShopItem {

    private String displayName;
    private int cost;
    private int slot;
    private ItemStack icon;
    private ItemStack product;
    private SpawnerType spawnerType;

    public ShopItem(String displayName, int cost, ItemStack icon, ItemStack product, SpawnerType spawnerType, int slot) {
        this.displayName = displayName;
        this.cost = cost;
        this.icon = icon;
        this.product = product;
        this.spawnerType = spawnerType;
        this.slot = slot;
    }

    @Override
    public String getDisplayName(MBPlayer player) {
        return this.displayName;
    }

    @Override
    public ItemStack getIcon(MBPlayer player) {
        return this.icon;
    }

    @Override
    public ItemStack getProduct(MBPlayer player) {
        return this.product;
    }

    @Override
    public SpawnerType getSpawnerType(MBPlayer player) {
        return this.spawnerType;
    }

    @Override
    public int getCost(MBPlayer player) {
        return this.cost;
    }

    @Override
    public int getSlot(MBPlayer player) {
        return this.slot;
    }
}
