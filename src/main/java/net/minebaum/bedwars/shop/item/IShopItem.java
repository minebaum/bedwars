package net.minebaum.bedwars.shop.item;

import net.minebaum.bedwars.spawners.SpawnerType;
import net.minebaum.playerapi.MBPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface IShopItem {

    String getDisplayName(MBPlayer player);

    ItemStack getIcon(MBPlayer player);
    ItemStack getProduct(MBPlayer player);

    SpawnerType getSpawnerType(MBPlayer player);

    int getCost(MBPlayer player);
    int getSlot(MBPlayer player);

}
