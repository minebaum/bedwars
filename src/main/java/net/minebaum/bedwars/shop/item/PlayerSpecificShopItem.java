package net.minebaum.bedwars.shop.item;

import net.minebaum.bedwars.spawners.SpawnerType;
import net.minebaum.playerapi.MBPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerSpecificShopItem implements IShopItem {

    private final Callable<String> displayNameCallable;
    private final Callable<ItemStack> iconCallable;
    private final Callable<ItemStack> productCallable;
    private final Callable<SpawnerType> spawnerTypeCallable;
    private final Callable<Integer> costCallable;
    private final Callable<Integer> slotCallable;

    public PlayerSpecificShopItem(Callable<String> displayNameCallable, Callable<ItemStack> iconCallable, Callable<ItemStack> productCallable, Callable<SpawnerType> spawnerTypeCallable, Callable<Integer> costCallable, Callable<Integer> slotCallable) {
        this.displayNameCallable = displayNameCallable;
        this.iconCallable = iconCallable;
        this.productCallable = productCallable;
        this.spawnerTypeCallable = spawnerTypeCallable;
        this.costCallable = costCallable;
        this.slotCallable = slotCallable;
    }

    public PlayerSpecificShopItem(String displayName, SpawnerType spawnerType, int cost, int slot, Callable<ItemStack> iconCallable, Callable<ItemStack> productCallable) {
        this.displayNameCallable = player -> displayName;
        this.spawnerTypeCallable = player -> spawnerType;
        this.costCallable = player -> cost;
        this.slotCallable = player -> slot;
        this.iconCallable = iconCallable;
        this.productCallable = productCallable;
    }

    @Override
    public String getDisplayName(MBPlayer player) {
        return this.displayNameCallable.call(player);
    }

    @Override
    public ItemStack getIcon(MBPlayer player) {
        return this.iconCallable.call(player);
    }

    @Override
    public ItemStack getProduct(MBPlayer player) {
        return this.productCallable.call(player);
    }

    @Override
    public SpawnerType getSpawnerType(MBPlayer player) {
        return this.spawnerTypeCallable.call(player);
    }

    @Override
    public int getCost(MBPlayer player) {
        return this.costCallable.call(player);
    }

    @Override
    public int getSlot(MBPlayer player) {
        return this.slotCallable.call(player);
    }

    private interface Callable<T> {
        T call(MBPlayer player);
    }
}
