package net.minebaum.bedwars.shop;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import net.minebaum.bedwars.shop.item.IShopItem;
import net.minebaum.bedwars.shop.item.ShopItem;
import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.developement.GuiAPI;
import net.minebaum.playerapi.developement.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

@Getter
public class Shop {

    // FIELDS
    private IShopMenu[] shopMenus;
    private String varianteName;
    private Inventory mainShop;
    private final MBPlayer player;

    // CONSTRUCTOR
    public Shop(final MBPlayer player, final String varianteName) {
        this.player = player;
        this.varianteName = varianteName;
        this.mainShop = new GuiAPI().fillerGUI(63, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 15).setDisplayname(" ").build(), ChatColor.RED + "BedWars-Shop");
    }

    public Shop buildShop() {
        for(final IShopMenu shopMenu : shopMenus){
            for(final IShopItem shopItem : shopMenu.initShopItems()) {
                ItemStack icon = shopItem.getIcon(this.player);
                ItemMeta meta = icon.getItemMeta();
                meta.setDisplayName(shopItem.getDisplayName(this.player));
                meta.setLore(Arrays.asList("§7Preis: " + shopItem.getCost(this.player) + " " + shopItem.getSpawnerType(this.player).getDisplayName()));
                icon.setItemMeta(meta);
                mainShop.setItem(shopItem.getSlot(this.player), icon);
            }
        }
        return this;
    }

    // STRING SEREALIZE
    public String toString(){
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
    public static Shop fromString(String from){
        return new Gson().fromJson(from, Shop.class);
    }
}
