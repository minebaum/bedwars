package net.minebaum.bedwars.bukkit.commands;

import net.minebaum.bedwars.game.GameManagement;
import net.minebaum.bedwars.counters.LobbyCountdown;
import net.minebaum.bedwars.states.LobbyState;
import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.developement.Data;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {return true;}
        MBPlayer player = new MBPlayer(((Player) commandSender));
        if(!player.hasPermission("system.bw.start")) {player.sendMessage(Data.NOPERMS); return true;}
        if(strings.length != 0) {player.sendMessage(Data.PREFIX + "§cNutze: /start"); return true;}
        if(GameManagement.getGame().getCurrentGameState().equals(GameManagement.getGame().getGameStates()[GameManagement.LOBBY_STATE])){
            if(((LobbyState) GameManagement.getGame().getCurrentGameState()).getCountdown().getSeconds() > 10){
                LobbyCountdown countdown = ((LobbyState) GameManagement.getGame().getCurrentGameState()).getCountdown();
                countdown.setSeconds(10);
                player.sendMessage(Data.PREFIX + "§aDer Countdown wurde erfolgreich verkürzt.");
                for(MBPlayer p : GameManagement.getGame().getPlayers()){
                    p.getSpigotPlayer().playSound(p.getSpigotPlayer().getLocation(), Sound.LEVEL_UP, 1, 1);
                }
            }else{
                player.sendMessage(Data.PREFIX  + "§cDu kannst so kurz vor dem Beginn, nicht mehr starten.");
            }
        }else{
            player.sendMessage(Data.PREFIX + "§cDies kannst du nur in der Lobby-Phase ausführen.");
        }
        return false;
    }
}
