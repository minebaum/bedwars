package net.minebaum.bedwars.bukkit.commands;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.map.Map;
import net.minebaum.bedwars.map.MapHandler;
import net.minebaum.bedwars.map.MapTeam;
import net.minebaum.bedwars.map.locations.MapLocation;
import net.minebaum.bedwars.spawners.SpawnerType;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;

public class SetupCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player player = ((Player) sender);
            MapHandler mapHandler = BedWars.getInstance().getMapHandler();
            if(!player.hasPermission("bedwars.setup")) return false;

            if(args.length == 4) {
                if(args[0].equalsIgnoreCase("createmap")) {

                    String name = args[1];
                    String displayName = ChatColor.translateAlternateColorCodes('&', args[2]);
                    String builder = args[3];

                    ItemStack iconStack = player.getItemInHand();
                    if(iconStack == null || iconStack.getType() == Material.AIR) {
                        player.sendMessage(BedWars.PREFIX + "§cBitte halte ein Item in der Hand, dass als Icon benutzt werden kann.");
                        return false;
                    }
                    mapHandler.addMap(new Map(name, displayName, builder, iconStack));
                    player.sendMessage(BedWars.PREFIX + "7Du hast die Map §e'" + displayName + "' §7erstellt.");
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("addteam")) {
                   String mapName = args[1];
                   String teamName = args[2];
                   Map map = mapHandler.getMapByName(mapName);
                   if(map == null) {
                       player.sendMessage(BedWars.PREFIX + "§cDiese Map existiert nicht.");
                       return false;
                   }
                   map.addTeam(new MapTeam(teamName));
                   player.sendMessage(BedWars.PREFIX + "§7Team hinzugefügt. Setzte die locations mit:");
                   player.sendMessage(BedWars.PREFIX + "§e/setup setspawn " + mapName + " " + teamName);
                   player.sendMessage(BedWars.PREFIX + "§e/setup setbed " + mapName + " " + teamName + " 1/2");
                } else if(args[0].equalsIgnoreCase("setbed")) {
                    String mapName = args[1];
                    String teamName = args[2];
                    Map map = mapHandler.getMapByName(mapName);
                    if(map == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDiese Map existiert nicht.");
                        return false;
                    }
                    MapTeam team = map.getMapTeamByName(teamName);
                    if(team == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDieses Team existiert nicht.");
                        return false;
                    }
                    Location location1 = player.getLocation().getBlock().getLocation();
                    Location location2 = this.getTargetBlock(player, 5).getLocation();
                    team.setBedLocation1(location1);
                    team.setBedLocation2(location2);
                    player.sendMessage(BedWars.PREFIX + "§7Bett Locations für das Team §e'" + team.getName() + "' gesetzt.");
                } else if(args[0].equalsIgnoreCase("setspawn")) {
                    String mapName = args[1];
                    String teamName = args[2];
                    Map map = mapHandler.getMapByName(mapName);
                    if(map == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDiese Map existiert nicht.");
                        return false;
                    }
                    MapTeam team = map.getMapTeamByName(teamName);
                    if(team == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDieses Team existiert nicht.");
                        return false;
                    }
                    Location location = player.getLocation();
                    team.setSpawnLocation(location);
                    player.sendMessage(BedWars.PREFIX + "§7Spawnlocation für das Team §e'" + team.getName() + "' gesetzt.");
                }else if(args[0].equalsIgnoreCase("addspawner")) {
                    String mapName = args[1];
                    Map map = mapHandler.getMapByName(mapName);
                    if(map == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDiese Map existiert nicht.");
                        return false;
                    }
                    if(this.isSpawnerType(args[2].toUpperCase())) {
                        SpawnerType type = SpawnerType.valueOf(args[2].toUpperCase());
                        MapLocation location = MapLocation.fromLocation(player.getLocation().clone().subtract(0, 1,0));
                        map.addSpawnerLocation(type, location);
                        player.sendMessage(BedWars.PREFIX + "§7Der Spawner (" + type.toString() + ") wurde gesetzt.");
                    }else {
                        player.sendMessage(BedWars.PREFIX + "Dieser Spawer-Typ existiert nicht. Es existieren folgende Typen: §eBRONZE, SILVER, GOLD, DIAMOND");
                    }
                }
            }else if(args.length == 2) {
                if(args[0].equalsIgnoreCase("setspectator")) {
                    String mapName = args[1];
                    Map map = mapHandler.getMapByName(mapName);
                    if (map == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDiese Map existiert nicht.");
                        return false;
                    }
                    map.setSpectatorLocation(MapLocation.fromLocation(player.getLocation()));
                    player.sendMessage(BedWars.PREFIX + "§7Spectatorlocation gesetzt für §e" + map.getDisplayName() + " §7gesetzt.");
                } else if(args[0].equalsIgnoreCase("finish")) {
                    String mapName = args[1];
                    Map map = mapHandler.getMapByName(mapName);
                    if (map == null) {
                        player.sendMessage(BedWars.PREFIX + "§cDiese Map existiert nicht.");
                        return false;
                    }
                    if(!map.canFinish()) {
                        player.sendMessage(BedWars.PREFIX + "§cDiese Map kann noch nicht fertig gestellt werden!");
                        return false;
                    }
                    map.finish();
                    player.sendMessage(BedWars.PREFIX + "§7Die Map §e" + map.getDisplayName() + " §7wurde gespeichert.");
                }
            }
        }
        return true;
    }

    private boolean isSpawnerType(String in) {
        try {
            SpawnerType.valueOf(in);
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    private Block getTargetBlock(Player player, int range) {
        BlockIterator iter = new BlockIterator(player, range);
        Block lastBlock = iter.next();
        while (iter.hasNext()) {
            lastBlock = iter.next();
            if (lastBlock.getType() == Material.AIR) {
                continue;
            }
            break;
        }
        return lastBlock;
    }

    //setup createmap <name> <displayname> <builder> -
    //setup setbed <map> <teamname> -
    //setup addTeam <map> <teamname> -
    //setup addSpawner <map> <spawner-typ> -
    //setup setspawn <map> <teamname> -
    //setup setspectator <map>
    //setup finish <map>

}
