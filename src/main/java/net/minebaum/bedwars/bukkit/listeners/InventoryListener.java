package net.minebaum.bedwars.bukkit.listeners;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.map.Map;
import net.minebaum.bedwars.map.voting.MapVoting;
import net.minebaum.bedwars.map.voting.MapVotingGui;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String inventoryName = player.getInventory().getName();
        ItemStack clickedItem = event.getCurrentItem();
        if(clickedItem == null || clickedItem.getType().equals(Material.AIR) || event.getSlotType().equals(InventoryType.SlotType.OUTSIDE)) return;

        //Map voting
        if(inventoryName.equals(MapVotingGui.GUI_NAME)) {
            event.setCancelled(true);
            Map map = BedWars.getInstance().getMapHandler().getMapByName(ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName()));
            if(map != null) {
                MapVoting voting = BedWars.getInstance().getMapHandler().getVoting();
                if(voting.hasVoted(player)) {
                    player.sendMessage(BedWars.PREFIX + "§7Du hast bereits für die Map §e" + voting.getMapOfPlayer(player).getDisplayName() + " §7abgestimmt.");
                    return;
                } else {
                    voting.addVote(player, map);
                }
            }else {
                player.closeInventory();
                player.sendMessage(BedWars.PREFIX + "§cMap nicht gefunden! Bitte melde diesen Bug beim Team!");
            }
        }
    }
}
