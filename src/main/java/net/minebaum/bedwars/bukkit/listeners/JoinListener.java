package net.minebaum.bedwars.bukkit.listeners;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.game.GameManagement;
import net.minebaum.bedwars.counters.LobbyCountdown;
import net.minebaum.bedwars.states.LobbyState;
import net.minebaum.playerapi.MBPlayer;
import net.minebaum.playerapi.developement.Data;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        MBPlayer p = new MBPlayer(e.getPlayer());
        if(GameManagement.getGame().getCurrentGameState().equals(GameManagement.getGame().getGameStates()[GameManagement.INGAME_STATE | GameManagement.ENDING_STATE])){
            GameManagement.getGame().getSpecHandler().setSpec(p);
            for(MBPlayer specs : GameManagement.getGame().getSpecHandler().getPlayers()){
                specs.sendMessage(Data.PREFIX + "§7Der Spieler §e" + p.getName() + " §7hat BedWars als Spectator betreten.");
            }
            e.setJoinMessage(null);
        }

        if(GameManagement.getGame().getCurrentGameState().equals(GameManagement.getGame().getGameStates()[GameManagement.LOBBY_STATE])){
            /*
            ///////////////////////////////// PREMIUM JOIN //////////////////////////////////////////////
             */
            if(GameManagement.getGame().getMaxPlayers() == GameManagement.getGame().getPlayers().size()){
                boolean alreadyDID = false;
                for(MBPlayer player : GameManagement.getGame().getPlayers()){
                    if(!player.hasPermission("system.premiumjoin") && !alreadyDID && p.hasPermission("system.premiumjoin")){
                        player.getSpigotPlayer().kickPlayer(Data.PREFIX + "§cDu wurdest von einem Spieler mit höherem Rang gekickt.");
                        e.setJoinMessage(Data.PREFIX + "§7Der Spieler §e" + p.getName() + " §7hat BedWars betreten.");
                        GameManagement.getGame().removePlayer(player);
                        GameManagement.getGame().addPlayer(p);
                        alreadyDID = true;
                        return;
                    }else{
                        if(!alreadyDID && p.hasPermission("system.premiumjoin")){
                            p.getSpigotPlayer().kickPlayer(Data.PREFIX + "§cDiese Runde ist bereits mit §ePremium §cSpielern gefüllt.");
                        }else if(!alreadyDID){
                            p.getSpigotPlayer().kickPlayer(Data.PREFIX + "§cDiese Runde ist breits voll.");
                        }
                    }
                }
            }else
                /*
                ////////////////////////////////////////// CASE >> NOT ALREADY MINPLAYERS ///////////////////////////////////////
                 */
                if(GameManagement.getGame().getMinPlayers() < GameManagement.getGame().getPlayers().size()){
                    GameManagement.getGame().addPlayer(p);
                    e.setJoinMessage(Data.PREFIX + "§7Der Spieler §e" + p.getName() + " §7hat BedWars betreten.");
                    LobbyState state = (LobbyState) GameManagement.getGame().getCurrentGameState();
                    LobbyCountdown countdown = state.getCountdown();
                    if(GameManagement.getGame().getMinPlayers() == GameManagement.getGame().getPlayers().size()){
                        countdown.stopIdle();
                        countdown.start();
                    }
                }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if(BedWars.getInstance().getMapHandler().getVoting().isRunning()) {
            BedWars.getInstance().getMapHandler().getVoting().removeVote(player);
        }
    }

}
