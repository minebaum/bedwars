package net.minebaum.bedwars.data;

import net.minebaum.playerapi.developement.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LobbyData {

    public static final ItemStack TEAM_SELECT = new ItemBuilder(Material.BED, 1, (short) 0).setDisplayname("§c§lTeamauswahl §7(Rechtsklick)").setUnbreakable().build();
    public static final ItemStack VOTING_ITEM = new ItemBuilder(Material.PAPER, 1, (short) 0).setDisplayname("§e§lVoting §7(Rechtsklick)").setUnbreakable().build();
    public static final ItemStack LEAE_ITEM = new ItemBuilder(Material.ARROW, 1, (short) 0).setDisplayname("§cZurück zur Lobby").setUnbreakable().build();

}
