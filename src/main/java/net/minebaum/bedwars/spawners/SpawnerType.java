package net.minebaum.bedwars.spawners;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

@Getter
public enum SpawnerType {

    BRONZE(new ItemStack(Material.AIR), "§cBronze", 20),
    SILVER(new ItemStack(Material.AIR), "§fSilber", 20 * 15),
    GOLD(new ItemStack(Material.AIR), "§6Gold", 20 * 30),
    DIAMOND(new ItemStack(Material.AIR), "§bDiamanten", 20 * 60);

    private final ItemStack stack;
    private final int spawnDelay;
    private final String displayName;

    SpawnerType(ItemStack stack, String displayName, int spawnDelay) {
        this.stack = stack;
        this.spawnDelay = spawnDelay;
        this.displayName = displayName;
    }

}
