package net.minebaum.bedwars.states;

import net.minebaum.bedwars.BedWars;
import net.minebaum.bedwars.counters.LobbyCountdown;
import net.minebaum.bedwars.map.Map;
import net.minebaum.playerapi.game.states.GameState;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class LobbyState extends GameState {

    private LobbyCountdown countdown;

    public LobbyState() {
        countdown = new LobbyCountdown();
    }

    @Override
    public void start() {
        countdown.startIdle();
    }

    @Override
    public void stop() {
        Map map = BedWars.getInstance().getMapHandler().getFinalMap();
        map.loadWorld();
    }

    public LobbyCountdown getCountdown() {
        return countdown;
    }


    public void set(Player p) {
        Scoreboard board = new Scoreboard();
        ScoreboardObjective objective = board.registerObjective("Test", IScoreboardCriteria.b);

        objective.setDisplayName("§c§lMineBaum §8| §7BedWars");
        board.setDisplaySlot(1, objective);

        PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(objective, 1);
        PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(objective, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, objective);

        sendPacket(p, removePacket);
        sendPacket(p, createPacket);
        sendPacket(p, display);
        sendScore(p, board, objective, " ", 10);
        sendScore(p, board, objective, "§7 | §cProfil ", 9);
        sendScore(p, board, objective, "§7 | × " + p.getName(), 8);
        sendScore(p, board, objective, "  ", 7);
        sendScore(p, board, objective, "§7 | §cVariante", 6);
        sendScore(p, board, objective, "§7 | × Duel", 5);
        sendScore(p, board, objective, "   ", 4);
        sendScore(p, board, objective, "§7 | §cMap", 3);
        sendScore(p, board, objective, "§7 | × §7Voting....", 2);
        sendScore(p, board, objective, "    ", 1);
    }

    @SuppressWarnings("rawtypes")
    private void sendPacket(Player p, Packet packet) {
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }

    private void sendScore(Player player, Scoreboard board, ScoreboardObjective obj, String score, int scoreint) {
        ScoreboardScore s = new ScoreboardScore(board, obj, score);
        s.setScore(scoreint);
        sendPacket(player, new PacketPlayOutScoreboardScore(s));
    }

}
